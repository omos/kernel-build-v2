#!/bin/bash

set -e
set -x

DISTGIT_URL="https://src.fedoraproject.org/rpms/kernel.git"

TEST_PATCH_NAME='test-patch.patch'
NAME_FILE_NAME='name.txt'
DIST_FILE_NAME='dist.txt'

SED_SCRIPT_ADD_PATCH='s/^\(# END OF PATCH DEFINITIONS\)$/Patch900: '"$TEST_PATCH_NAME"'\n\n\1/g'
SED_SCRIPT_CONFIG='s/^\(\.\/process_configs\.sh\) \$OPTS/\1/g'

dnf install git fedpkg

git clone "$DISTGIT_URL" kernel

(
cd kernel

git checkout "$(cat "../$DIST_FILE_NAME")"

cat \
	<(echo "%global buildid .$(cat "../$NAME_FILE_NAME")") \
	<(sed -e "$SED_SCRIPT_ADD_PATCH" -e "$SED_SCRIPT_CONFIG" kernel.spec) \
	>kernel.spec.new

mv kernel.spec.new kernel.spec
cp "../$TEST_PATCH_NAME" .

fedpkg srpm

mv *.src.rpm "$OUT"
)

# remove junk symlinks from outdir:
find "$OUT" -maxdepth 1 -type l -exec rm -f {} \;
