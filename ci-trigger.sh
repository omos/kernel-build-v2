#!/bin/bash

set -e
set -x

REPO_URL="https://gitlab.com/omos/linux-public.git"
REPO_PATH="cache/linux-public"

TEST_PATCH_NAME='test-patch.patch'
NAME_FILE_NAME='name.txt'
DIST_FILE_NAME='dist.txt'

[ "$EVENT_PUSH" = 1 ] || exit 0

[ -d "$REPO_PATH" ] || git clone -n "$REPO_URL" "$REPO_PATH"

(cd "$REPO_PATH" && git tag | xargs -n 1 git tag --delete && git fetch origin --tags)

git config --global user.name "Kernel Buildbot"
git config --global user.email "omosnacek+kernel-buildbot@gmail.com"

git remote set-url origin "https://gitlab-ci-token:$PUSH_TOKEN@gitlab.com/omos/kernel-build-v2.git"

function parse_commitish() {
	(cd "$REPO_PATH" && git rev-parse --verify "$1") || \
	(cd "$REPO_PATH" && git rev-parse --verify "origin/$1")
}

function process_tag() {
	local tag="$1"
	local commit="$2"

	local name="$(cut -d / -f 2 <<<"$tag")"
	local base="$(cut -d / -f 3 <<<"$tag")"
	local dist="$(cut -d / -f 4 <<<"$tag")"

	local commit_head="$(parse_commitish "$tag")"
	local commit_base="$(parse_commitish "$base")"

	[ -n "$commit_head" ] && [ -n "$commit_base" ]

	local build_branch="build/$dist/$name/$commit_base-$commit_head"

	if git show-ref --verify --quiet "refs/remotes/origin/$build_branch"; then
		return 0
	fi

	git checkout -b "$build_branch" master

	(cd "$REPO_PATH" && git format-patch --stdout "$commit_base".."$commit_head") \
		>"$TEST_PATCH_NAME"

	echo "$name" >"$NAME_FILE_NAME"
	echo "$dist" >"$DIST_FILE_NAME"

	git add "$TEST_PATCH_NAME" "$NAME_FILE_NAME" "$DIST_FILE_NAME"

	git diff --staged

	{
	echo "[buildbot] $name: $base..$tag (on $dist)"
	echo
	echo "Head commit: $commit_head"
	echo "Base commit: $commit_base"
	} | git commit -F -
	git push -u origin "$build_branch" || return 0
}

(cd "$REPO_PATH" && git tag -l --format '%(refname:strip=2) %(objectname)' 'build/*') | \
	while read LINE; do process_tag $LINE; done
